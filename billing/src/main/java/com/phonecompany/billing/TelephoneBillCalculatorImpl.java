package com.phonecompany.billing;

import java.math.BigDecimal;
import java.util.List;

public class TelephoneBillCalculatorImpl implements TelephoneBillCalculator {

    public BigDecimal calculate(String phoneLog) {
        List<Call> allCalls = CallParser.parseCallsFromCSV(phoneLog);
        List<Call> filteredCalls = TelephoneBillHelper.filterMostCalledPhoneNumberOut(allCalls);

        BigDecimal price = BigDecimal.ZERO;
        for (Call call : filteredCalls) {
            price = price.add(TelephoneBillHelper.calculatePriceOfCall(call));
        }

        return price;
    }


}
