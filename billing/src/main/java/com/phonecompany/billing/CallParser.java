package com.phonecompany.billing;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CallParser {

    public static List<Call> parseCallsFromCSV (String csv) {
        String[] lines = csv.split("\n");
        List<Call> allCalls = new ArrayList<>();
        for (String line : lines) {
            String[] callStrings = line.split(",");
            Long phoneNumber = Long.parseLong(callStrings[0]);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            LocalDateTime callStart = LocalDateTime.parse(callStrings[1], formatter);
            LocalDateTime callEnd = LocalDateTime.parse(callStrings[2], formatter);
            Call call = new Call(phoneNumber, callStart, callEnd);
            allCalls.add(call);
        }

        return allCalls;
    }

}
