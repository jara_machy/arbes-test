package com.phonecompany.billing;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TelephoneBillHelper {

    public static final BigDecimal MINUTE_PRICE_CLASSIC = BigDecimal.valueOf(1);
    public static final BigDecimal MINUTE_PRICE_DISCOUNT = BigDecimal.valueOf(0.5);
    public static final BigDecimal MINUTE_PRICE_LONG_CALL = BigDecimal.valueOf(0.2);

    public static final long SCHEDULED_PRICE_MINUTES_LIMIT = 5;

    public static List<Call> filterMostCalledPhoneNumberOut(List<Call> allCalls) {
        HashMap<Long, Integer> phoneNumberCounts = createPhoneNumberCountHashMap(allCalls);
        Long mostCalledPhoneNumber = findMostCalledPhoneNumber(phoneNumberCounts);
        List<Call> filteredCalls = filterPhoneNumberOut(allCalls, mostCalledPhoneNumber);

        return filteredCalls;
    }

    public static BigDecimal calculatePriceOfCall(Call call) {
        BigDecimal sumPriceClassic = BigDecimal.ZERO;
        BigDecimal sumPriceDiscount = BigDecimal.ZERO;
        BigDecimal sumPriceExtraMinutes;

        LocalDateTime callStart = call.getCallStart();
        LocalDateTime callEnd = call.getCallEnd();
        long secondsDuration = Duration.between(callStart, callEnd).toSeconds();
        long fullMinutes = secondsDuration / 60;
        long seconds = secondsDuration % 60;
        long minutes;
        if (seconds > 0) {
            minutes = fullMinutes + 1;
        } else {
            minutes = fullMinutes;
        }

        long scheduledPriceMinutes;
        if (minutes > SCHEDULED_PRICE_MINUTES_LIMIT) {
            scheduledPriceMinutes = SCHEDULED_PRICE_MINUTES_LIMIT;
            sumPriceExtraMinutes = BigDecimal.valueOf(minutes - 5).multiply(MINUTE_PRICE_LONG_CALL);
        } else {
            scheduledPriceMinutes = minutes;
            sumPriceExtraMinutes = BigDecimal.ZERO;
        }

        long startingHour = callStart.getHour();
        long startingMinute = callStart.getMinute();
        long minutesTillEndOfHour = 60 - startingMinute;

        if (startingHour <= 6 || startingHour >= 16) {
            sumPriceClassic = BigDecimal.ZERO;
            sumPriceDiscount = BigDecimal.valueOf(scheduledPriceMinutes).multiply(MINUTE_PRICE_DISCOUNT);
        } else if (startingHour >= 8 && startingHour <= 14) {
            sumPriceClassic = BigDecimal.valueOf(scheduledPriceMinutes).multiply(MINUTE_PRICE_CLASSIC);
            sumPriceDiscount = BigDecimal.ZERO;
        } else if (startingHour == 7) {
            if (minutesTillEndOfHour < scheduledPriceMinutes) {
                sumPriceDiscount = BigDecimal.valueOf(minutesTillEndOfHour).multiply(MINUTE_PRICE_DISCOUNT);
                sumPriceClassic = BigDecimal.valueOf(scheduledPriceMinutes-minutesTillEndOfHour).multiply(MINUTE_PRICE_CLASSIC);
            } else {
                sumPriceClassic = BigDecimal.ZERO;
                sumPriceDiscount = BigDecimal.valueOf(scheduledPriceMinutes).multiply(MINUTE_PRICE_DISCOUNT);
            }
        } else if (startingHour == 15) {
            if (minutesTillEndOfHour < scheduledPriceMinutes) {
                sumPriceClassic = BigDecimal.valueOf(minutesTillEndOfHour).multiply(MINUTE_PRICE_CLASSIC);
                sumPriceDiscount = BigDecimal.valueOf(scheduledPriceMinutes-minutesTillEndOfHour).multiply(MINUTE_PRICE_DISCOUNT);
            } else {
                sumPriceDiscount = BigDecimal.ZERO;
                sumPriceClassic = BigDecimal.valueOf(scheduledPriceMinutes).multiply(MINUTE_PRICE_CLASSIC);
            }
        }

        BigDecimal priceOfCall = sumPriceClassic.add(sumPriceDiscount.add(sumPriceExtraMinutes));

        return priceOfCall;
    }

    // PRIVATE METHODS

    private static HashMap<Long, Integer> createPhoneNumberCountHashMap(List<Call> allCalls) {
        HashMap<Long, Integer> phoneNumberCounts = new HashMap<>();

        for (Call call : allCalls) {
            Long phoneNumber = call.getPhoneNumber();
            phoneNumberCounts.merge(phoneNumber, 1, Integer::sum);
        }

        return phoneNumberCounts;
    }

    private static Long findMostCalledPhoneNumber(HashMap<Long, Integer> phoneNumberCounts) {
        int max = Collections.max(phoneNumberCounts.values());
        List<Long> keys = phoneNumberCounts.entrySet().stream()
                .filter(entry -> entry.getValue() == max)
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());

        return Collections.max(keys);
    }

    private static List<Call> filterPhoneNumberOut(List<Call> allCalls, Long phoneNumber) {
        List<Call> filteredCalls = new ArrayList<>();
        for (Call call : allCalls) {
            if (!call.getPhoneNumber().equals(phoneNumber)) {
                filteredCalls.add(call);
            }
        }
        return filteredCalls;
    }
}
