package com.phonecompany.billing;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Call {

    private Long phoneNumber;
    private LocalDateTime callStart;
    private LocalDateTime callEnd;

}
