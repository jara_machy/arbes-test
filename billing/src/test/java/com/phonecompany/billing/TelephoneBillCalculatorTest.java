package com.phonecompany.billing;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TelephoneBillCalculatorTest {

    @Test
    public void testCalculate() {
        TelephoneBillCalculator tbc = new TelephoneBillCalculatorImpl();

        BigDecimal price = tbc.calculate("" +
                "420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57\n" + // 1,5
                "420776562400,18-01-2020 08:59:20,18-01-2020 09:10:00\n" + // 6,2
                "420776562400,18-01-2020 07:55:20,18-01-2020 07:59:19\n" + // 2
                "420776562400,18-01-2020 07:55:20,18-01-2020 07:59:20\n" + // 2
                "420776562500,18-01-2020 08:59:20,18-01-2020 09:10:00\n" + // will be ignored - most called number
                "420776562500,18-01-2020 06:55:20,18-01-2020 08:59:19\n" + // will be ignored - most called number
                "420776562500,18-01-2020 04:55:20,18-01-2020 07:59:20\n" + // will be ignored - most called number
                "420776562353,18-01-2020 07:58:20,18-01-2020 08:01:33\n" + // 3
                "420776562353,18-01-2020 07:58:20,18-01-2020 08:05:33\n" + // 4,6
                "420776562363,18-01-2020 15:57:20,18-01-2020 16:05:11\n" + // 4,6
                "420776562363,18-01-2020 15:50:20,18-01-2020 15:59:11\n" + // 5,8
                "420776562363,18-01-2020 23:59:01,19-01-2020 00:01:36\n" + // 1,5
                "");
        assertEquals(BigDecimal.valueOf(31.2), price);
    }
}
