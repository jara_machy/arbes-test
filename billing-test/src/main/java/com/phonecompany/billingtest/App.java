package com.phonecompany.billingtest;

import com.phonecompany.billing.TelephoneBillCalculator;
import com.phonecompany.billing.TelephoneBillCalculatorImpl;

import java.math.BigDecimal;

public class App {

    public static void main(String[] args) {
        TelephoneBillCalculator telephoneBillCalculator = new TelephoneBillCalculatorImpl();
        BigDecimal finalPrice = telephoneBillCalculator.calculate("" +
                "420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57\n" +
                "420776562353,18-01-2020 08:59:20,18-01-2020 09:10:00\n" +
                "420776562111,18-01-2020 07:55:20,18-01-2020 07:59:19\n" +
                "420776562598,18-01-2020 07:55:20,18-01-2020 07:59:20\n" + // 2
                "420776562785,18-01-2020 07:58:20,18-01-2020 08:01:33\n" + // 3
                "420776562999,18-01-2020 07:58:20,18-01-2020 08:05:33\n" + // 4,6
                "420776562111,18-01-2020 08:59:20,18-01-2020 09:10:00\n");
        System.out.println(finalPrice);
    }
}
